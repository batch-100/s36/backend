const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')

// [SECTION] Primary Routes

router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))
})

router.post('/', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
    //result is just a parameter, callback function
})

router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})
//uung callback function dito ung req,res, second argument lang pala siya which is naka function, 
////////////////////////////////
// anything inside the METHOD is an argument
//param is always related in creation of function, always tatanggap siya ng value
//argument siya if gagamitin na natin siya with info
//pag tinawag na siya, invoking a function
//ginagamit lang si .then if we have something to do with the info, functions na nagrereturn ng promise
//if function nagcocommunicate, its a promise
/////////////////////////////////

router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})

router.post('/enroll', auth.verify, (req, res) => {
	const params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
    UserController.enroll(params).then(result => res.send(result))
})

// [SECTION] Secondary Routes

router.put('/details', (req, res) => {
    UserController.updateDetails()
})

router.put('/change-password', (req, res) => {
    UserController.changePassword()
})

// [SECTION] Integration Routes

router.post('/verify-google-id-token', (req, res) => {
    UserController.verifyGoogleTokenId()
})

module.exports = router